/** @type {import('tailwindcss').Config} */

module.exports = {
  theme: {
    container: {
      center: true,
    },
    safelist: [
      'bg-orange-400',
      'bg-green-400',
      'bg-blue-400',
      'bg-yellow-400',
      'bg-pink-400',
    ],
    fontSize: {
      xs: '0.75rem',
      sm: '0.875rem',
      base: '1rem',
      l: '1.125rem',
      xl: '1.25rem',
      '2xl': '1.5rem',
      '3xl': '2.5rem',
      '4xl': '3.75rem',
      '5xl': '4.5rem',
    },
  },
};
